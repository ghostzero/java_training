/**
 * Created by ghost on 30.10.14.
 */
public class BoolTest {

//    int x=123_456_789;

    public static void main(String args[]) {
//            int x = 0b10101;
//        byte z = 22;

        boolean b;
        b = false;
        System.out.println("b равна " + b);
        b = true;
        System.out.println("b равна " + b);

        //значение типа может управлять оператором if
        if (b) System.out.println("Это выполняеться.");
        b = false;
        if (b) System.out.println("Это не выполняеться");
        //результат сравнения - значения типа boolean
        System.out.println("10 > 9 равно " + (10 > 9));
    }
}
