/**
 * Created by ghost on 29.09.15.
 */
//Компиляция этой программы невозможна
public class ScopeErr {
    public static void main(String args[]) {
        int bar=1;
        {
//            int bar=2;//создание новой области видимости. Ошибка времени компиляции - переменная bar уже определена!
        }
    }
}
