/**
 * Created by ghost on 29.09.15.
 */
public class Promote {
    public static void main(String args[]) {
        /*byte a = 40;
        byte b = 50;
        byte c = 100;
        int d = a * b / c;
        System.out.println(d);*/

        /*byte b = 50;
        b = b * 2; Ошибка! Значение типа не может быть присвоено переменной типа byte!
        */
        /*byte b = 50;
        b = (byte) (b * 2);*/

        byte b = 42;
        char c = 'c';
        short s = 1024;
        int i = 50000;
        float f = 5.67f;
        double d = .1234;
        double result = (f * b) + (i / c) - (d * s);
        System.out.println((f * b) + "+" + (i / c) + "-" + (d * s));
        System.out.println("result = " + result);
    }
}
