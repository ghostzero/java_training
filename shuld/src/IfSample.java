/**
 * Created by ghost on 19.10.14.
 *
 * Демонстрирует применение оператора if
 *
 */
public class IfSample {
    public static void main (String arg[]){
        int x,y;
        x=10;
        y=20;
        if(x<y) System.out.println("x меньше у");
//x=x*2;
        x*=2;
        if(x==y) System.out.println("x теперь равно у");
        x*=2;
        if(x>y) System.out.println("x теперь больше у");
        // этот оператор не будет ничего отображать
        if(x==y) System.out.println("x теперь равно у");

    }
}
