/**
 * Created by ghost on 28.09.15.
 */
//Демонстрация времени существования переменной.
public class LifeTime {
    public static void main(String args[]) {
        int x;

        for (x = 0; x < 3; x++) {
            int y = -1;//y инициализируеться при каждом вхождении
                        // в блок
            System.out.println("y равна: " + y);// эта строка всегда выводит значенипе -1
            y = 100;
            System.out.println("y теперь равна: " + y);
        }
    }
}
