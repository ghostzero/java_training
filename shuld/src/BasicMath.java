/**
 * Created by ghost on 02.10.15.
 */
public class BasicMath {
    public static void main(String args[]) {
//            арифметические операции с челочисленными значениями
        System.out.println("Целочисленная арифметика");
        int a = 1 + 1;
        int b = a * 3;
        int c = b / 4;
        int d = c - a;
        int e = -d;
        System.out.println("a=" + a);
        System.out.println("a=" + b);
        System.out.println("a=" + c);
        System.out.println("a=" + d);
        System.out.println("a=" + e);

//арифметические операции со значениями типа double
        System.out.println("\nАрифметические операции с плавающей точкой");
        double da = 1 + 1;
        double db = da * 3;
        double dc = db / 4;
        double dd = dc - da;
        double de = -dd;
        System.out.println("da = " + da);
        System.out.println("db = " + db);
        System.out.println("dc = " + dc);
        System.out.println("dd = " + dd);
        System.out.println("de = " + de);
    }
}
